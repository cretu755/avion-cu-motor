#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>
#include "Quaternion.h"
#include "camera.h"
#include "MyCamera.h"

class MyTransform {
public:
	MyTransform(glm::vec3 position, glm::vec3 scale, float yaw, float pitch, float roll) {
		m_position = position;
		m_scale = scale;
		m_quaternion = new Quaternion(yaw, pitch, roll);
	}
	~MyTransform();

	glm::vec3 GetPos() { return m_position; }
	void SetPos(glm::vec3 pos) { m_position = pos; }

	glm::vec3 GetScale() { return m_scale; }
	void SetScale(glm::vec3 scale) { m_scale = scale; }

	glm::mat4 TransposeMat4(glm::mat4 mat) {
		glm::mat4 rez;

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				rez[i][j] = mat[j][i];

		return rez;
	}

	glm::mat4 GetModel() {
		return glm::translate(m_position) * glm::scale(m_scale) * TransposeMat4(m_quaternion->QuatToMat()); 
	}

	glm::mat4 GetProjectedModel(Camera* camera){
		return camera->getViewProjection() * GetModel();
	}

	glm::mat4 GetProjectedModel(MyCamera* camera) {
		return camera->getViewProjection() * GetModel();
	}

	Quaternion* GetQuaternion() { return m_quaternion; }

	void rotate(float yaw, float pitch, float roll) {
		m_quaternion = new Quaternion(yaw, pitch, roll);
	}

	float distance(glm::vec3 pos) {
		return std::sqrt(((m_position.x * m_position.x) - (pos.x * pos.x)) +
								((m_position.x * m_position.y) - (pos.x * pos.y)) +
								((m_position.z * m_position.z) - (pos.z * pos.z)));
	}

	float distance2p(glm::vec3 pos1, glm::vec3 pos2) {
		return std::sqrt(((pos1.x * pos1.x) - (pos2.x * pos2.x)) +
			((pos1.x * pos1.y) - (pos2.x * pos2.y)) +
			((pos1.z * pos1.z) - (pos2.z * pos2.z)));
	}

	glm::vec3 Lerp(glm::vec3 v1, glm::vec3 v2, float ammount) {
		glm::vec3 rez = glm::vec3(0, 0, 0);

		rez.x = v1.x + (v2.x - v1.x) * ammount;
		rez.y = v1.y + (v2.y - v1.y) * ammount;
		rez.z = v1.z + (v2.z - v1.z) * ammount;

		return rez;
	}

private:
	glm::vec3 m_position;
	glm::vec3 m_scale;
	Quaternion* m_quaternion;
};