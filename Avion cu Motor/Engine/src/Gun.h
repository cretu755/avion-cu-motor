#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>
#include "Bullet.h"

class Gun {
public:
	float m_fire_rate = 0.0f;
	int m_mag_size = 10;

	Gun(MyTransform* transform);
	~Gun();

	void Shoot(MyTransform* transform);

	Bullet** GetBullets();

private:
	Bullet** m_bullets;
};