#include "3Dobject.h"
#include "input.h"
#include <iostream>
#include <cmath>
#include "phong_shader.h"

object3D::object3D(std::string mesh_path, std::string texture_path) {
	m_mesh = new Mesh(mesh_path);
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));
	m_transform = new Transform();
	m_transform->setScale(glm::vec3(0.5f, 0.5f, 0.5f));
	m_transform->setPos(glm::vec3(0, 0, 0));
}

object3D::object3D(std::string mesh_path, std::string texture_path, glm::vec3* pos, glm::vec3* rot, glm::vec3* scale) {
	m_mesh = new Mesh(mesh_path);
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));
	m_transform = new Transform();
	m_transform->setPos(*pos);
	m_transform->setRot(*rot);
	m_transform->setScale(*scale);
}

object3D::object3D(Mesh* mesh, glm::vec3* pos, glm::vec3* rot, glm::vec3* scale, std::string texture_path) {
	m_mesh = mesh;
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));
	m_transform = new Transform();
	m_transform->setPos(*pos);
	m_transform->setRot(*rot);
	m_transform->setScale(*scale);
}

Material* object3D::GetMaterial() { return m_material; }
void object3D::SetMaterial(Material* material) { m_material = material; }

Transform* object3D::GetTransform() { return m_transform; }
void object3D::SetTransform(Transform* transform) { m_transform = transform; }

Mesh* object3D::GetMesh() { return m_mesh; }
void object3D::SetMesh(Mesh* mesh) { m_mesh = mesh; }

Shader* object3D::GetShader() { return m_shader; }
void object3D::SetShader(Shader* shader) { m_shader = shader; }

void object3D::update() {
	
}

void object3D::render(Camera* camera) {
	PhongShader::getInstance()->bind();
	PhongShader::getInstance()->updateUniforms(m_transform->getModel(), m_transform->getProjectedModel(camera), m_material, camera->getPos());
	m_mesh->draw();
}