#include "MyCamera.h"
#include "input.h"

float yaw = 0;
float pitch = 0;
float sensitivity = 0.4f;

MyCamera::MyCamera(const glm::f32vec3 pos, float fov, float aspect, float zNear, float zFar, float yaw, float pitch, float roll){
	m_position = pos;
    m_perspective = glm::perspective(fov, aspect, zNear, zFar);
    m_forward = glm::f32vec3(0, 0, 1);
    m_up = glm::f32vec3(0, 1, 0);
    m_mouse_pos = glm::vec2(0, 0);
    m_yaw = yaw;
    m_pitch = pitch;
    m_roll = roll;
    m_quaternion = new Quaternion(yaw, pitch, roll);
}

glm::mat4 MyCamera::getViewProjection(){
    return m_perspective * glm::lookAt(m_position, m_position + m_forward, m_up);
}

glm::vec3 MyCamera::getPos() {
    return m_position;
}

void MyCamera::translate(glm::vec3 displace) { m_position += displace; }

void MyCamera::SetPos(glm::vec3 pos) { m_position = pos; }

glm::vec3 MyCamera::GetForward() { return m_forward; }

void MyCamera::SetForward(glm::vec3 forward) {
    m_forward = forward;
}

void MyCamera::SetUp(glm::vec3 up) {
    m_up = up;
}

Quaternion* MyCamera::GetQuaternion() { return m_quaternion; }

void MyCamera::CameraMovement(glm::vec2 curent_mouse_pos) {
    
}

void MyCamera::rotate(float yaw, float pitch, float roll) {
    m_quaternion = new Quaternion(yaw, pitch, roll);
}

void MyCamera::input() {
    CameraMovement(Input::getMousePos());
}

