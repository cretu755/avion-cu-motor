#include "Avion.h"
#include "time.h"
#include "TransformMatrix.h"
#include <math.h>
#include "Bullet.h"

float yaw_rate = 1.0f;
float roll_rate = 3.0f;
float pitch_rate = 1.0f;
float plane_speed = 0.0008f;
float roll_ratio = 0;
float yaw_ratio = 0;
float pitch_ratio = 0;

bool moving = false;

Avion::Avion(std::string mesh_path, std::string texture_path, float yaw, float pitch, float roll, Game* game, int mag_size) {
	m_mesh = new Mesh(mesh_path);
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));

	m_trans = new MyTransform(glm::vec3(0, 0, 0), glm::vec3(0.5f, 0.5f, 0.5f), yaw, pitch, roll);
	m_yaw = yaw;
	m_pitch = pitch;
	m_roll = roll;
	m_forward = m_trans->GetQuaternion()->Forward();

	m_camera = new MyCamera(m_trans->GetPos() - glm::vec3(0, -0.5f, 2), 70, (float)1200 / 670, 1.0f, 10000.0f, yaw, pitch, roll);
	m_camera->SetForward(m_camera->GetQuaternion()->Forward());
	m_camera->SetUp(m_camera->GetQuaternion()->Up());

	m_game = game;

	m_gun = new Gun(m_trans);

	m_hitbox = new PlaneHitBox(m_trans);

	m_renderMatrix = glm::mat4(1);
}

MyCamera* Avion::GetCamera() { return m_camera; }

void Avion::update() {
	glm::vec3 pos_avion = m_trans->GetPos();
	glm::vec3 pos_avion_ac = m_trans->GetPos();
	glm::vec3 new_camera_pos;
	glm::vec3 camera_pos_ac = m_camera->getPos();

	m_forward = m_trans->GetQuaternion()->Forward();
	if(moving)
		pos_avion_ac = pos_avion + (plane_speed * m_forward);
	m_trans->SetPos(pos_avion_ac);

	new_camera_pos = m_trans->GetPos() - 4.0f * m_forward;
	new_camera_pos.y += 0.5f;
	new_camera_pos = m_trans->Lerp(camera_pos_ac, new_camera_pos, Time::getDelta() * 4);
	m_camera->SetPos(new_camera_pos);
	m_camera->SetForward(m_camera->GetQuaternion()->Forward());
	m_camera->SetUp(m_camera->GetQuaternion()->Up());

	m_hitbox->update(plane_speed, m_trans);

	m_renderMatrix = glm::mat4(1);
	m_renderMatrix *= glm::translate(m_trans->GetPos());
	m_renderMatrix *= m_trans->TransposeMat4(m_trans->GetQuaternion()->QuatToMat());
	m_renderMatrix *= glm::scale(m_trans->GetScale());
}

void Avion::input() {
	if (Input::getMouse(SDL_MOUSEBUTTONDOWN)) {
		m_gun->Shoot(m_trans);
	}

	if (Input::getKeyDown(SDLK_c))
		moving = true;

	if (Input::getKeyDown(SDLK_v))
		moving = false;

	if (Input::getKey(SDLK_d)) {
		m_yaw -= (1 - roll_ratio) * yaw_rate * Time::getDelta();
		m_camera->m_yaw -= (1 - roll_ratio) * yaw_rate * Time::getDelta();
		m_pitch += roll_ratio * pitch_rate * Time::getDelta();
		m_camera->m_pitch += roll_ratio * pitch_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);
		m_camera->rotate(m_camera->m_yaw, m_camera->m_pitch, m_camera->m_roll);
	}

	if (Input::getKey(SDLK_a)) {
		m_yaw += (1 - roll_ratio) * yaw_rate * Time::getDelta();
		m_camera->m_yaw += (1 - roll_ratio) * yaw_rate * Time::getDelta();
		m_pitch -= roll_ratio * pitch_rate * Time::getDelta();
		m_camera->m_pitch -= roll_ratio * pitch_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);
		m_camera->rotate(m_camera->m_yaw, m_camera->m_pitch, m_camera->m_roll);
	}

	if (Input::getKey(SDLK_w)) {
		m_yaw -= roll_ratio * yaw_rate * Time::getDelta();
		m_camera->m_yaw -= roll_ratio * yaw_rate * Time::getDelta();
		m_pitch -= (1 - roll_ratio) * pitch_rate * Time::getDelta();
		m_camera->m_pitch -= (1 - roll_ratio) * pitch_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);
		m_camera->rotate(m_camera->m_yaw, m_camera->m_pitch, m_camera->m_roll);
	}

	if (Input::getKey(SDLK_s)) {
		m_yaw += roll_ratio * yaw_rate * Time::getDelta();
		m_camera->m_yaw += roll_ratio * yaw_rate * Time::getDelta();
		m_pitch += (1 - roll_ratio) * pitch_rate * Time::getDelta();
		m_camera->m_pitch += (1 - roll_ratio) * pitch_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);
		m_camera->rotate(m_camera->m_yaw, m_camera->m_pitch, m_camera->m_roll);
	}

	if (Input::getKey(SDLK_q)) {
		roll_ratio = 0;
		m_roll -= roll_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);

		if (m_roll > M_PI / 2)
			m_roll = M_PI / 2;
		if (m_roll < -M_PI / 2)
			m_roll = -M_PI / 2;

		roll_ratio = std::abs(m_roll) / (M_PI / 2);
	}

	if (Input::getKey(SDLK_e)) {
		roll_ratio = 0;
		m_roll += roll_rate * Time::getDelta();
		m_trans->rotate(m_yaw, m_pitch, m_roll);

		if (m_roll > M_PI / 2)
			m_roll = M_PI / 2;
		if (m_roll < -M_PI / 2)
			m_roll = -M_PI / 2;

		roll_ratio = std::abs(m_roll) / (M_PI / 2);
	}
}