#pragma once
#include <GL\glew.h>
#include <cstddef>
#include "mesh.h"
#include "shader.h"
#include "transform.h"
#include "material.h"
#include "MyCamera.h"
#include "MyTransform.h"
#include "phong_shader.h"
#include "MyMesh.h"

class VertexObj
{
public:

	MyTransform* m_trans;

	VertexObj();
	VertexObj(CMesh* mesh, glm::vec3 pos, glm::vec3 scale, std::string texture_path) {
		m_mesh = mesh;

		m_trans = new MyTransform(pos, scale, 0, 0, 0);

		m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));
	}

	~VertexObj();

	CMesh* GetMesh() { return m_mesh; }
	void SetMesh(CMesh* mesh) { m_mesh = mesh; }

	//Transform* GetTransform(){ return m_transform; }
	//void SetTransform(Transform* transform){ m_transform = transform; }

	MyTransform* GetMyTransform() { return m_trans; }

	Material* GetMaterial() { return m_material; }
	void SetMaterial(Material* material) { m_material = material; }

	Shader* GetShader() { return m_shader; }
	void SetShader(Shader* shader) { m_shader = shader; }

	void render(Camera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(GetRenderMat(), camera->getViewProjection() * GetRenderMat(), m_material, camera->getPos());
		m_mesh->draw();
	}

	void render(MyCamera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(GetRenderMat(), camera->getViewProjection() * GetRenderMat(), m_material, camera->getPos());
		m_mesh->draw();
	}

	glm::mat4 GetRenderMat() {
		return glm::translate(m_trans->GetPos()) * glm::scale(m_trans->GetScale()) * m_trans->TransposeMat4(m_trans->GetQuaternion()->QuatToMat());
	}

private:
	CMesh* m_mesh;
	Shader* m_shader;
	//Transform* m_transform;
	Material* m_material;
	float m_yaw;
	float m_pitch;
	float m_roll;
	glm::mat4 m_renderMatrix;
};