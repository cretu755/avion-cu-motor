/*
   A test game engine with C++ and OpenGL
   Copyright (C) 2015  Sadman Kazi

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   */

#include "game.h"
#include "input.h"
#include <iostream>
#include "time.h"
#include <cmath>
#include "phong_shader.h"
#include "Avion.h"
#include "Stationar.h"
#include "AvionAI.h"
#include "HeightMap.h"
#include "MyMesh.h"
#include "VertexObj.h"

#include <stdlib.h>

Avion* avion;
AvionAI** ais;
Stationar* skybox;
object3D* triunghi;
Mesh* mesh;
CMesh* cmesh;
VertexObj* tri;
HeightMap* teren;

bool debug = false;
bool lock_skybox = false;

int nr_ai = 1;

Game::Game()
{
    Vertex data[] = { Vertex(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec2(0, 0)),
        Vertex(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec2(0,0)),
        Vertex(glm::vec3(0.0f, 1.0f, 0.0f),glm::vec2(0,0)) };

    glm::vec3 verts1[] = { glm::vec3(1.0f, 0.0f, 1.0f),
                        glm::vec3(0.0f, 0.0f, 1.0f),
                        glm::vec3(1.0f, 0.0f, 0.0f),
                        glm::vec3(0.0f, 0.0f, 1.0f),
                        glm::vec3(1.0f, 0.0f, 0.0f),
                        glm::vec3(0.0f, 0.0f, 0.0f) };

    glm::vec3* verts = new glm::vec3[300];

    int start = 6;
    int end = -6;
    int vert_index = 0;

    for (int j = start; j > end; j--) {
        for (int i = start; i > end; i--) {
            verts[vert_index] = glm::vec3((float)i, 0.0f, (float)j);
            vert_index++;
            verts[vert_index] = glm::vec3((float)i - 1, 0.0f, (float)j);
            vert_index++;
            verts[vert_index] = glm::vec3((float)i, 0.0f, (float)j - 1);
            vert_index++;
            verts[vert_index] = glm::vec3((float)i - 1, 0.0f, (float)j);
            vert_index++;
            verts[vert_index] = glm::vec3((float)i, 0.0f, (float)j - 1);
            vert_index++;
            verts[vert_index] = glm::vec3((float)i - 1, 0.0f, (float)j - 1);
            vert_index++;
        }
    }

    ais = new AvionAI * [nr_ai];

    //mesh = new Mesh(data, sizeof(data) / sizeof(data[0]), indices, sizeof(indices) / sizeof(indices[0]));

    //cmesh = new CMesh(verts1, sizeof(verts1) / sizeof(verts1[0]));
    cmesh = new CMesh(verts, vert_index);

    skybox = new Stationar("../res/Skybox.obj", "../res/skybox.jpg", glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(500, 500, 500));
    avion = new Avion("../res/Avion.obj", "../res/Avion.jpg", 0, 0, 0, this, 10);

    for(int i = 0; i < nr_ai; i++)
        ais[i] = new AvionAI("../res/Avion.obj", "../res/Avion.jpg", 0, 0, 0);

    //teren = new HeightMap("../res/AvionTeren.raw", 100, 100, 513, 513);

    //triunghi = new object3D(mesh, glm::vec3(1, 0, 1), glm::vec3(0.5f, 0.5f, 0.5f), "../res/brwon_checker.jpg");
    tri = new VertexObj(cmesh, glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), "../res/brwon_checker.jpg");

    debug_camera = new Camera(glm::vec3(0.0f, 0.0f, -2.0f), 70.0f, (float)WIDTH/HEIGHT, 1.0f, 10000.0f);

    m_counter = 0;
    //glDisable(GL_CULL_FACE);
}


Game::~Game()
{
    delete debug_camera;
}

void Game::input() {
    if (debug)
        debug_camera->input();

    if (Input::getKeyDown(SDLK_p)) {
        lock_skybox = true;
    }

    if (Input::getKeyDown(SDLK_o)) {
        lock_skybox = false;
    }

    if (Input::getKeyDown(SDLK_z)){
        debug = false;
    }
    
    if (Input::getKeyDown(SDLK_x)) {
        debug = true;
    }

    if(!debug)
        avion->input();
    
    for(int i = 0; i < nr_ai; i++)
        if(!ais[i]->dead)
            ais[i]->AI();
}

void Game::render() {
    if (debug) {
        avion->render(debug_camera);

        for(int i = 0; i < nr_ai; i++)
            ais[i]->render(debug_camera);

        glCullFace(GL_FRONT);
        skybox->render(debug_camera);
        glCullFace(GL_BACK);

        for (int i = 0; i < avion->m_gun->m_mag_size; i++)
            if (avion->m_gun->GetBullets()[i]->active)
                avion->m_gun->GetBullets()[i]->render(debug_camera);

        glDisable(GL_CULL_FACE);
        tri->render(debug_camera);
        glEnable(GL_CULL_FACE);
    }
    else {
        avion->render(avion->GetCamera());
        
        for (int i = 0; i < nr_ai; i++)
            ais[i]->render(avion->GetCamera());

        glCullFace(GL_FRONT);
        skybox->render(avion->GetCamera());
        glCullFace(GL_BACK);

        for (int i = 0; i < avion->m_gun->m_mag_size; i++)
            if (avion->m_gun->GetBullets()[i]->active)
                avion->m_gun->GetBullets()[i]->render(avion->GetCamera());

        glDisable(GL_CULL_FACE);
        tri->render(avion->GetCamera());
        glEnable(GL_CULL_FACE);
    }
    /*if (Input::getKeyDown(SDLK_UP)) {
      std::cout << "You have pressed up!" << std::endl;
      m_camera->translate(glm::vec3(0.0f, 0.0f, 0.1f));
      }
      else if (Input::getKeyUp(SDLK_UP)) {
      std::cout << "You have released up!" << std::endl;
      }

      if (Input::getMouseDown(SDL_BUTTON_LEFT)) {
      std::cout << "X: " << Input::getMousePos().x << " Y: " << Input::getMousePos().y << std::endl;
      }
      else if (Input::getMouseUp(SDL_BUTTON_LEFT)) { std::cout << "You released the left mouse button!" << std::endl;
      }
      */
}

void Game::update() {
    m_counter += Time::getDelta();
    //std::cout << counter << std::endl;
    //float sinCounter = sin(m_counter);
    //float absSinCounter = abs(sinCounter);

    //m_transform->getRot().x = sinCounter;
    //avion->GetTransform()->getScale().y -= 0.0002f;
    //m_transform->getRot().z = sinCounter;
    avion->update();

    if(lock_skybox)
        skybox->update(avion->GetCamera());

    for (int i = 0; i < avion->m_gun->m_mag_size; i++)
        if (avion->m_gun->GetBullets()[i]->active)
            avion->m_gun->GetBullets()[i]->update();

    for (int i = 0; i < nr_ai; i++) {
        ais[i]->update();
    }

    Collision();

    //avion->GetShader()->updateUniforms(avion->GetTransform()->getModel(), avion->GetTransform()->getProjectedModel(m_camera), avion->GetMaterial(), m_camera->getPos());
    //skybox->GetShader()->updateUniforms(skybox->GetTransform()->getModel(), skybox->GetTransform()->getProjectedModel(m_camera), skybox->GetMaterial(), m_camera->getPos());
}

void Game::Collision() {
    for (int i = 0; i < avion->m_gun->m_mag_size; i++)
        if (avion->m_gun->GetBullets()[i]->active)
            for (int j = 0; j < nr_ai; j++)
                if (ais[j]->GetHitBox()->active &&
                    avion->m_trans->distance2p(avion->m_gun->GetBullets()[i]->m_trans->GetPos(), ais[j]->m_trans->GetPos()) <= 0.5f)
                    if (ResolveCollision(ais[j]->GetHitBox(), avion->m_gun->GetBullets()[i])) {
                        avion->m_gun->GetBullets()[i]->active = false;
                        ais[j]->hp -= avion->m_gun->GetBullets()[i]->damage;
                    }
}

bool Game::ResolveCollision(PlaneHitBox* ai_hitbox, Bullet* bullet) {
    for (int i = 0; i < ai_hitbox->sfere_nr; i++)
        if (avion->m_trans->distance2p(ai_hitbox->GetSfere()[i]->m_trans->GetPos(), bullet->m_trans->GetPos()) >= 0.3f)
            return true;

    return false;
}

void Game::loadMesh(const std::string& model_path) {

}
