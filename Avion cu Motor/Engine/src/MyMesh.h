#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>

class CMesh {
public:
	CMesh(glm::vec3* verts, int num_verts);
	~CMesh();

	void draw();
	GLuint loadShaders(const char* vertex_file_path, const char* fragment_file_path);
private:
	int num_vertices;

	GLuint vertexbuffer;
	GLuint VertexArrayID;
	GLuint programID;
};