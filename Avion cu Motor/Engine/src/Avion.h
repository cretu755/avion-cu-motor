#pragma once
#include "3Dobject.h"
#include "MyCamera.h"
#include "phong_shader.h"
#include "game.h"
#include "Bullet.h"
#include "Gun.h"
#include "PlaneHitBox.h"

class Avion : public object3D {
public:
	Gun* m_gun;

	Avion();
	Avion(std::string mesh, std::string textur, float yaw, float pitch, float roll, Game* game, int mag_size);
	~Avion();

	void input();
	MyCamera* GetCamera();

	void update();

	void render(Camera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_renderMatrix, camera->getViewProjection() * m_renderMatrix, m_material, camera->getPos());
		m_mesh->draw();

		//m_hitbox->render(camera);
	}

	void render(MyCamera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_renderMatrix, camera->getViewProjection() * m_renderMatrix, m_material, camera->getPos());
		m_mesh->draw();

		//m_hitbox->render(camera);
	}

	glm::vec3 m_forward = glm::vec3(0, 0, 1);
protected:
	MyCamera* m_camera;
	Game* m_game;
	PlaneHitBox* m_hitbox;
};