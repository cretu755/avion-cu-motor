#pragma once
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>

glm::mat3 GenRotMatZ(float turn_rate) {
	return glm::mat3( cos(turn_rate), -sin(turn_rate), 0,
						sin(turn_rate), cos(turn_rate), 0,
						0, 0, 1 );
}

glm::mat3 GenRotMatY(float turn_rate) {
	return glm::mat3( cos(turn_rate), 0, sin(turn_rate),
							0, 1, 0,
							-sin(turn_rate), 0, cos(turn_rate) );
}

glm::mat3 GenRotMatX(float turn_rate) {
	return glm::mat3( 1, 0, 0,
						0, cos(turn_rate), -sin(turn_rate),
						0, sin(turn_rate), cos(turn_rate) );
}

glm::vec3 Multiply(glm::mat3 mat, glm::vec3 vec) {
	glm::vec3 rez = glm::vec3 (0, 0, 0);
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			rez[i] += mat[i][j] * vec[j];

	return rez;
}