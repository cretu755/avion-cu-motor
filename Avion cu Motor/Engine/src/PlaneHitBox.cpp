#include "PlaneHitBox.h"
#include "3Dobject.h"

PlaneHitBox::PlaneHitBox(MyTransform* trans) {
	sfere = new Stationar * [sfere_nr];
	
	sfere[0] = new Stationar("../res/skybox.obj", "../res/bricks.jpg", trans->GetPos(), glm::vec3(0, 0, 0), glm::vec3(0.3f, 0.3f, 0.3f));
	sfere[1] = new Stationar("../res/skybox.obj", "../res/bricks.jpg", trans->GetPos() + glm::vec3(0, 0, 0.6f), glm::vec3(0, 0, 0), glm::vec3(0.3f, 0.3f, 0.3f));
	sfere[2] = new Stationar("../res/skybox.obj", "../res/bricks.jpg", trans->GetPos() + glm::vec3(0, 0, -0.6f), glm::vec3(0, 0, 0), glm::vec3(0.3f, 0.3f, 0.3f));
}

Stationar** PlaneHitBox::GetSfere() { return sfere; }

void PlaneHitBox::render(Camera* camera) {
	for (int i = 0; i < sfere_nr; i++)
		sfere[i]->render(camera);
}

void PlaneHitBox::render(MyCamera* camera) {
	for (int i = 0; i < sfere_nr; i++)
		sfere[i]->render(camera);
}

void PlaneHitBox::update(float speed, MyTransform* trans) {
	sfere[0]->m_trans->SetPos(trans->GetPos());
	sfere[1]->m_trans->SetPos(trans->GetPos() + (0.6f * trans->GetQuaternion()->Forward()));
	sfere[2]->m_trans->SetPos(trans->GetPos() + (-0.6f * trans->GetQuaternion()->Forward()));
}