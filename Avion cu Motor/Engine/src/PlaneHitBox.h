#pragma once
#include "Stationar.h"

class PlaneHitBox{
public:
	int sfere_nr = 3;

	bool active = true;

	PlaneHitBox(MyTransform* trans);
	~PlaneHitBox();

	void render(Camera* camera);
	void render(MyCamera* camera);

	void update(float speed, MyTransform* trans);

	Stationar** GetSfere();

private:
	Stationar** sfere;
};