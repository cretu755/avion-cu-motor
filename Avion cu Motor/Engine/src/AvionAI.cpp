#include "AvionAI.h"
#include "time.h"
#include <stdlib.h>
#include <time.h>

float speed = 6.0f;
float turn_rate_ai = 1.0f;
float turn_angle_yaw = 0;
float turn_angle_pitch = 0;
float death_angle = -0.5f;

int turn_dir_yaw;
int turn_dir_pitch;

bool retreat = false;
bool just_turned = false;

glm::vec3 turn_point;

AvionAI::AvionAI(std::string mesh_path, std::string texture_path, float yaw, float pitch, float roll) {
	m_mesh = new Mesh(mesh_path);
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));

	int x = rand() % 5;
	int y = rand() % 5;
	int z = rand() % 5;

	m_trans = new MyTransform(glm::vec3(x, y, z), glm::vec3(0.5f, 0.5f, 0.5f), yaw, pitch, roll);
	m_yaw = yaw;
	m_pitch = pitch;
	m_roll = roll;
	m_forward = m_trans->GetQuaternion()->Forward();

	m_hitbox = new PlaneHitBox(m_trans);
}

PlaneHitBox* AvionAI::GetHitBox() { return m_hitbox; }

void AvionAI::Death() {
	if (std::abs(m_pitch - death_angle) >= 0.1f) {
		m_pitch += turn_rate_ai * Time::getDelta();
	}

	m_yaw += turn_rate_ai * Time::getDelta();

	m_trans->rotate(m_yaw, m_pitch, m_roll);
}

void AvionAI::update() {

	if (dead)
		Death();

	glm::vec3 pos_avion_ac = glm::vec3(0, 0, 0);
	glm::vec3 pos_avion = m_trans->GetPos();

	m_forward = m_trans->GetQuaternion()->Forward();
	pos_avion_ac = pos_avion + ((speed * (float)Time::getDelta()) * m_forward);
	m_trans->SetPos(pos_avion_ac);

	m_hitbox->update(speed, m_trans);

	if (hp <= 0) {
		dead = true;
		m_hitbox->active = false;
	}

	if (m_trans->distance2p(m_trans->GetPos(), glm::vec3(0, 0, 0)) >= 50)
		render_bool = false;

	m_renderMatrix = glm::mat4(1);
	m_renderMatrix *= glm::translate(m_trans->GetPos());
	m_renderMatrix *= m_trans->TransposeMat4(m_trans->GetQuaternion()->QuatToMat());
	m_renderMatrix *= glm::scale(m_trans->GetScale());
}

void AvionAI::AI() {
	if (just_turned)
		if (m_trans->distance(turn_point) >= 1)
			just_turned = false;

	if(!retreat && !just_turned)
		if (m_trans->distance(glm::vec3(0, 0, 0)) >= 30) {
			retreat = true;
			srand(time(NULL));
			turn_angle_yaw = rand() % 1 + 3;
			//turn_angle_pitch = rand() % 2 + 3;
			turn_dir_pitch = rand() % 200 + 1;
			turn_dir_yaw = rand() % 200 + 1;
		}

	if (retreat) {
		if (turn_angle_pitch > 0) {
			turn_angle_pitch -= turn_rate_ai * Time::getDelta();
			if (turn_dir_pitch > 100)
				m_pitch -= turn_rate_ai * Time::getDelta();
			else
				m_pitch += turn_rate_ai * Time::getDelta();
		}

		if (turn_angle_yaw > 0) {
			turn_angle_yaw -= turn_rate_ai * Time::getDelta();
			if (turn_dir_yaw > 100)
				m_yaw -= turn_rate_ai * Time::getDelta();
			else
				m_yaw += turn_rate_ai * Time::getDelta();
		}

		m_trans->rotate(m_yaw, m_pitch, m_roll);

		if (turn_angle_pitch <= 0 && turn_angle_yaw <= 0) {
			retreat = false;
			just_turned = true;
			turn_point = m_trans->GetPos();
		}
	}
}
