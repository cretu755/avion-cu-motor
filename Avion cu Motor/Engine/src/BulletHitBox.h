#pragma once
#include "Stationar.h"

class BulletHitBox {
public:

	BulletHitBox(MyTransform* trans);
	~BulletHitBox();

	void update(MyTransform* trans);
	void render(Camera* camera);
	void render(MyCamera* camera);

	Stationar* GetSfera();

private:

	Stationar* sfera;
};