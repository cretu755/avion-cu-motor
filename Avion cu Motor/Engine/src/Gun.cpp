#include "Gun.h"
#include "time.h"

Gun::Gun(MyTransform* transform) {
	m_bullets = new Bullet * [m_mag_size];

	for (int i = 0; i < m_mag_size; i++)
		m_bullets[i] = new Bullet(0, 0, 0, transform);
}

Bullet** Gun::GetBullets() { return m_bullets; }

void Gun::Shoot(MyTransform* transform) {
	if (m_fire_rate <= 0) {
		for (int i = 0; i < m_mag_size; i++)
			if (!m_bullets[i]->active) {
				m_bullets[i]->active = true;
				m_bullets[i]->SetForward(transform->GetQuaternion()->Forward());
				m_bullets[i]->m_start_point = transform->GetPos();
				m_bullets[i]->SetPos(transform->GetPos());
				m_bullets[i]->m_trans->SetScale(m_bullets[i]->m_default_scale);
				m_bullets[i]->m_in_range = true;
				m_fire_rate = 0.05f;
				break;
			}
	}

	m_fire_rate -= Time::getDelta();
}