#include "Stationar.h"

Stationar::Stationar(std::string mesh_path, std::string texture_path, glm::vec3 pos, glm::vec3 rot, glm::vec3 scale) {
	m_mesh = new Mesh(mesh_path);
	m_trans = new MyTransform(pos, scale, 0, 0, 0);
	m_material = new Material(new Texture(texture_path), new glm::vec3(1, 1, 1));
}

void Stationar::update(MyCamera* camera) {
	m_trans->SetPos(camera->getPos());
	/*m_renderMatrix = glm::mat4(1);
	m_renderMatrix *= glm::translate(m_trans->GetPos());
	m_renderMatrix *= m_trans->TransposeMat4(m_trans->GetQuaternion()->QuatToMat());
	m_renderMatrix *= glm::scale(m_trans->GetScale());*/
}