#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>

class Quaternion{
public:
	float x;
	float y;
	float z;
	float w;

	Quaternion(float yaw, float pitch, float roll) {
        const float half_yaw = yaw * 0.5f;
        const float half_pitch = pitch * 0.5f;
        const float half_roll = roll * 0.5f;
        const float sy = sinf(half_yaw);
        const float cy = cosf(half_yaw);
        const float sp = sinf(half_pitch);
        const float cp = cosf(half_pitch);
        const float sr = sinf(half_roll);
        const float cr = cosf(half_roll);

        x = ((cy * sp) * cr) + ((sy * cp) * sr);
        y = ((sy * cp) * cr) - ((cy * sp) * sr);
        z = ((cy * cp) * sr) - ((sy * sp) * cr);
        w = ((cy * cp) * cr) + ((sy * sp) * sr);
	}

    glm::mat4 QuatToMat() {
        float qx = x;
        float qy = y;
        float qz = z;
        float qw = w;

        const float n = 1.0f / sqrt(qx * qx + qy * qy + qz * qz + qw * qw);
        qx *= n;
        qy *= n;
        qz *= n;
        qw *= n;

        return glm::mat4(1.0f - 2.0f * qy * qy - 2.0f * qz * qz, 2.0f * qx * qy - 2.0f * qz * qw, 2.0f * qx * qz + 2.0f * qy * qw, 0.0f,
            2.0f * qx * qy + 2.0f * qz * qw, 1.0f - 2.0f * qx * qx - 2.0f * qz * qz, 2.0f * qy * qz - 2.0f * qx * qw, 0.0f,
            2.0f * qx * qz - 2.0f * qy * qw, 2.0f * qy * qz + 2.0f * qx * qw, 1.0f - 2.0f * qx * qx - 2.0f * qy * qy, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f);
    }

    glm::vec3 Forward()
    {
        return glm::vec3(-2.f * (z * x + y * w), 2.f * (x * w - y * z), 2.f * (x * x + y * y) - 1.f);
    }

    glm::vec3 Up() {
        glm::vec3 up = glm::vec3(0, 0, 1);

        up.x = 2 * (x * y - w * z);
        up.y = 1 - 2 * (x * x + z * z);
        up.z = 2 * (y * z + w * x);

        return up;
    }

    glm::vec3 Left() {
        glm::vec3 left = glm::vec3(0, 0, 0);

        left.x = 1 - 2 * (y * y + z * z);
        left.y = 2 * (x * y + w * z);
        left.z = 2 * (w * z - w * y);

        return left;
    }
};