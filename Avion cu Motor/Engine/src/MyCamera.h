#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>
#include "Quaternion.h"

class MyCamera {
public:
	float m_yaw;
	float m_pitch;
	float m_roll;

	MyCamera(const glm::f32vec3 pos, float fov, float aspect, float zNear, float zFar, float yaw, float pitch, float roll);
	~MyCamera();

	void translate(glm::vec3 displace);
	void input();
	glm::mat4 getViewProjection();
	void CameraMovement(glm::vec2 curent_mouse_pos);

	glm::vec3 getPos();
	void SetPos(glm::vec3 pos);

	glm::vec3 GetForward();
	void SetForward(glm::vec3 forward);
	void SetUp(glm::vec3 up);

	Quaternion* GetQuaternion();

	void rotate(float yaw, float pitch, float roll);

private:
	glm::mat4 m_perspective;
	glm::f32vec3 m_position;
	glm::f32vec3 m_forward;
	glm::f32vec3 m_up;
	glm::vec2 m_mouse_pos;
	Quaternion* m_quaternion;
};