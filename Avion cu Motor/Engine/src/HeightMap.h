#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>
#include <string>
#include <GL/glew.h>

class HeightMap {
public:
	HeightMap(const char* file_name, int row, int colums, int width, int height, 
		bool withPositions = true, bool withTextureCoordinates = true, bool withNormals = true) {

		m_row = row;
		m_height = height;
		m_width = width;
		m_height = height;

		m_heightData = new float*[m_row];
		for (int i = 0; i < m_row; i++)
			m_heightData[i] = new float[m_colums];

		getHeightDataFromImage(file_name);
		if (m_height_data_size == 0) {
			return;
		}

		createFromHeightData(m_heightData);
	}
	~HeightMap();

	void createFromHeightData(float** heightData) {
		glGenVertexArrays(1, &m_vao);
		glBindVertexArray(m_vao);
		
	}

	void render();
	void renderPoints();

	int getRows() const;
	int getColumns() const;
	float getHeight(const int row, const int column) const;

	void getHeightDataFromImage(const char* file_name) {
		FILE* file;
		fopen_s(&file, file_name, "r");

		fread(m_heightData, m_width, m_height, file);

		fclose(file);
	}

	void setUpVertices();
	void setUpTextureCoordinates();
	void setUpNormals();
	void setUpIndexBuffer();

private:
	int m_row;
	int m_colums;
	int m_height_data_size = 0;
	int m_width;
	int m_height;
	int m_bytes_per_pix;

	bool hasTexture;
	bool hasPosiiton;
	bool hasNormal;

	float** m_heightData;

	glm::vec3** m_vetices;
	glm::vec3** m_normals;

	glm::vec2** m_textureCoordinates;

	GLuint m_vao;
};