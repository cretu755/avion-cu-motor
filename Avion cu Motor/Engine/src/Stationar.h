#pragma once
#include "3Dobject.h"
#include "phong_shader.h"


class Stationar : public object3D {
public:
	Stationar(std::string mesh_path, std::string texture_path, glm::vec3 pos, glm::vec3 rot, glm::vec3 scale);
	~Stationar();

	void render(Camera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_trans->GetModel(), m_trans->GetProjectedModel(camera), m_material, camera->getPos());
		m_mesh->draw();
	}

	void render(MyCamera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_trans->GetModel(), m_trans->GetProjectedModel(camera), m_material, camera->getPos());
		m_mesh->draw();
	}

	void update(MyCamera* camera);
};