#include "BulletHitBox.h"

BulletHitBox::BulletHitBox(MyTransform* trans) {
	sfera = new Stationar("../res/skybox.obj", "../res/bricks.jpg", trans->GetPos(), glm::vec3(0, 0, 0), glm::vec3(0.3f, 0.3f, 0.3f));
}

Stationar* BulletHitBox::GetSfera() { return sfera; }

void BulletHitBox::render(Camera* camera) {
	sfera->render(camera);
}

void BulletHitBox::render(MyCamera* camera) {
	sfera->render(camera);
}

void BulletHitBox::update(MyTransform* trans) {
	sfera->m_trans->SetPos(trans->GetPos());
}