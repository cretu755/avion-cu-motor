#pragma once
#include "3Dobject.h"
#include "phong_shader.h"
#include "PlaneHitBox.h"

class AvionAI : public object3D {
public:
	glm::vec3 m_forward;

	int hp = 100;

	bool render_bool = true;
	bool dead = false;

	AvionAI(std::string mesh, std::string textur, float yaw, float pitch, float roll);
	~AvionAI();

	void render(Camera* camera) {
		if (render_bool) {
			PhongShader::getInstance()->bind();
			PhongShader::getInstance()->updateUniforms(m_renderMatrix, camera->getViewProjection() * m_renderMatrix, m_material, camera->getPos());
			m_mesh->draw();
		}

		//m_hitbox->render(camera);
	}

	void render(MyCamera* camera) {
		if (render_bool) {
			PhongShader::getInstance()->bind();
			PhongShader::getInstance()->updateUniforms(m_renderMatrix, camera->getViewProjection() * m_renderMatrix, m_material, camera->getPos());
			m_mesh->draw();
		}

		//m_hitbox->render(camera);
	}

	PlaneHitBox* GetHitBox();

	void Death();

	void update();

	void AI();
private:
	PlaneHitBox* m_hitbox;
};