#pragma once

#include "MyTransform.h"
#include "3Dobject.h"
#include "phong_shader.h"
#include "time.h"
#include "BulletHitBox.h"

class Bullet : public object3D {
public:
	std::string bullet_mesh = "../res/bullet.obj";
	std::string bullet_texture = "../res/bricks.jpg";

	float m_proj_speed = 30.0f;
	float m_range = 5.0f;
	float m_active_time = 0;

	glm::vec3 m_start_point;
	glm::vec3 m_default_scale = glm::vec3(0.1f, 0.1f, 0.1f);

	bool m_in_range = true;
	bool active = false;

	int damage = 10;

	Bullet() {

	}

	Bullet(float yaw, float pitch, float roll, MyTransform* trans) {
		m_mesh = new Mesh(bullet_mesh);
		m_material = new Material(new Texture(bullet_texture), new glm::vec3(1, 1, 1));

		m_trans = new MyTransform(glm::vec3(0, 0, 0), glm::vec3(0.1f, 0.1f, 0.1f), yaw, pitch, roll);
		m_yaw = yaw;
		m_pitch = pitch;
		m_roll = roll;

		m_forward = trans->GetQuaternion()->Forward();

		m_start_point = trans->GetPos();
	}

	~Bullet() {

	}

	glm::vec3 GetForward() { return m_forward; }
	void SetForward(glm::vec3 forward) { m_forward = forward; }

	bool GetInRange() { return m_in_range; }
	void SetInRange(bool range) { m_in_range = range; }

	void SetPos(glm::vec3 pos) { m_trans->SetPos(pos); }

	void render(Camera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_trans->GetModel(), m_trans->GetProjectedModel(camera), m_material, camera->getPos());
		m_mesh->draw();
	}

	void render(MyCamera* camera) {
		PhongShader::getInstance()->bind();
		PhongShader::getInstance()->updateUniforms(m_trans->GetModel(), m_trans->GetProjectedModel(camera), m_material, camera->getPos());
		m_mesh->draw();
	}

	void update() {
		glm::vec3 new_pos;

		if (m_active_time >= 0.5f) {
			active = false;
			m_active_time = 0;
		}

		if (active) {
			new_pos = m_trans->GetPos();
			new_pos += (float)(m_proj_speed * Time::getDelta()) * m_forward;
			m_trans->SetPos(new_pos);
			m_trans->SetScale(m_trans->GetScale() - (glm::vec3(0.1f, 0.1f, 0.1f) * (float)Time::getDelta()));
			m_active_time += Time::getDelta();
		}
	}

private:
	glm::vec3 m_forward;
};