#include <Windows.h>
#include <iostream>

#include "Engine/src/coreEngine.h"
#include "Engine/src/Bullet.h"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow) {
    CoreEngine game(TITLE, WIDTH, HEIGHT);

    game.start();

    return 0;
}